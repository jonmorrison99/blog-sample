<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <title>Blog - Sample Dashboard</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">

    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="/styles/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/styles/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/styles/custom.css" rel="stylesheet" type="text/css"/>

{{--    @section('page-level-css')--}}
    {{--@show--}}

    <link rel="shortcut icon" href="favicon.ico">
</head>

<body class="page-md">

<div class="page-header">
    <div class="page-header-top">
        <div class="container">
            <a href="javascript:;" class="menu-toggler"></a>
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            {{--<span class="username username-hide-mobile">{{{ $user['email'] }}}</span>--}}
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="extra_profile.html">
                                    <i class="icon-user"></i> My Profile </a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="/auth/logout">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="page-header-menu">
        <div class="container">
            <div class="hor-menu ">
                <ul class="nav navbar-nav">
                    <li class="{{ isset($section['dashboard']) ? 'active' : '' }}"><a href="/dashboard">Dashboard</a></li>

                    @if(Auth::user()->type == 1)
                        <li class="{{ isset($section['articles']) ? 'active' : '' }}"><a href="/dashboard/articles" class="tooltips" data-container="body" data-placement="bottom" data-html="true" data-original-title="Articles">Articles</a></li>
                        <li class="{{ isset($section['users']) ? 'active' : '' }}"><a href="/dashboard/users" class="tooltips" data-container="body" data-placement="bottom" data-html="true" data-original-title="Users">Users</a></li>
                    @else
                        <li class="{{ isset($section['articles']) ? 'active' : '' }}"><a href="/articles" class="tooltips" data-container="body" data-placement="bottom" data-html="true" data-original-title="Articles">Articles</a></li>
                        <li class="{{ isset($section['balace']) ? 'active' : '' }}"><a href="/dashboard/balance" class="tooltips" data-container="body" data-placement="bottom" data-html="true" data-original-title="Balance">Balance</a></li>
                    @endif

                    <li class="{{ isset($section['password']) ? 'active' : '' }}"><a href="/dashboard/password" class="tooltips" data-container="body" data-placement="bottom" data-html="true" data-original-title="Change Password">Change Password</a></li>
                    <li class="{{ isset($section['logout']) ? 'active' : '' }}"><a href="/logout" class="tooltips" data-container="body" data-placement="bottom" data-html="true" data-original-title="Logout">Logout</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>



    <div class="page-container">
        <div class="page-content">
            <div class="container">
                <div class="row margin-top-10">
                    <div class="col-sm-12">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-footer">
        <div class="container">
            <strong>&copy; <?php echo date('Y'); ?> Blood Sweat & Tea</strong> All rights reserved.
        </div>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>

    @section('scripts')
        @include('scripts')
    @show

    <script>
        $(document).ready(function($) {
    //            Metronic.init(); // init metronic core componets
    //            Layout.init(); // init layout
    //            Demo.init(); // init demo(theme settings page)
//            Index.init(); // init index page
//            Tasks.initDashboardWidget(); // init tash dashboard widget
//            UIExtendedModals.init();
//            TableManaged.init();
        });
    </script>

{{--@section('init-script')--}}
@show

</body>
</html>