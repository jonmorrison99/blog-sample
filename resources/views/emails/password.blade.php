Hi {{ $user['name'] }}, you account has been created.

We need you to validate your email address. Click on the link to validate

{{ url('user/validate/'.$user['email']) }}
