@extends('home/app')

@section('content')
<link href="/css/error.css" rel="stylesheet" type="text/css"/>

<!-- BEGIN LOGO -->
<div class="logo">
    {{--<a href="/"><img src="{{{ $data['site']['logo'] }}}" alt=""/></a>--}}
</div>
<!-- END LOGO -->

<div class="row">
    <div class="col-md-12 page-404">
        <div class="number">
            405
        </div>
        <div class="details">
            <h3>Oops! Method Now Allowed</h3>
            <p>
                <a href="/">
                    Return home </a>
                or try the search bar below.
            </p>
            <form action="javascript:;">
                <div class="input-group input-medium">
                    <input type="text" class="form-control" placeholder="keyword...">
					<span class="input-group-btn">
					<button type="submit" class="btn blue"><i class="fa fa-search"></i></button>
					</span>
                </div>
                <!-- /input-group -->
            </form>
        </div>
    </div>
</div>
@endsection
