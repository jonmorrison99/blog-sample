<html>
	<head>
		<link href='//fonts.googleapis.com/css?family=Lato:10,300' rel='stylesheet' type='text/css'>

		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #B0BEC5;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 72px;
				margin-bottom: 40px;
			}

			.message {
				color: #000;
				margin-bottom: 20px;
				font-weight: 300;
			}

			.error {
				text-align: left;
				color: #000;
				font-weight: 300;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<div class="title">505 Internal Server Error</div>
				<div class="message">Email deliver is not enabled at the moment, if you could be so kind as to send this on to lionel@onpbg.com. Thanks</div>
				<div class="error">
					Line #: {{ $error->line }}<br />
					File: {{ $error->file }}<br />
					Error: {{ $error->message }}<br />
				</div>
			</div>
		</div>
	</body>
</html>
