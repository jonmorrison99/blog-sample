@extends('app')

@section('content')

    <div class="portlet light vyna-content">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject theme-font bold uppercase">Articles</span>
{{--                <span class="caption-helper">{{ count($index) }} items</span>--}}
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                <tr>
                    <th>Email</th>
                    <th>Joined</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Tools</th>
                </tr>
                </thead>
                <tbody>

                    @foreach ($data as $item)
                        <tr class="odd gradeX">
                            <td>{{ $item->email }}</td>
                            <td class="center">{{ $item->joined }}</td>
                            <td class="status">
                                @if($item->type==0)
                                    <button class="btn btn-info btn-xs blog-status-public" data-page="{{ $item->id }}">User</button>
                                @elseif($item->type==1)
                                    <button class="btn btn-success btn-xs blog-status-private" data-page="{{ $item->id }}">Admin</button>
                                @endif
                            </td>
                            <td class="status">
                                @if($item->status==0)
                                    <button class="btn btn-info btn-xs blog-status-public" data-page="{{ $item->id }}">New</button>
                                @elseif($item->status==1)
                                    <button class="btn btn-success btn-xs blog-status-private" data-page="{{ $item->id }}">Activated</button>
                                @elseif($item->status==2)
                                    <button class="btn btn-warning btn-xs blog-status-revoked" data-page="{{ $item->id }}">Suspended</button>
                                @else
                                    <button class="btn btn-danger btn-xs blog-status-deleted" data-page="{{ $item->id }}">Deleted</button>
                                @endif
                            </td>
                            <td>
                                <div>
                                    @if($item->status<=2)
                                        <a href="/dashboard/users/view/{{ $item->id }}" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>
                                    @endif

                                    @if($item->status==2)
                                        <a href="/dashboard/users/enable/{{ $item->id }}" class="btn btn-success btn-xs"><i class="fa fa-check"></i></a>
                                    @endif

                                    @if($item->status<=1)
                                        <a href="/dashboard/users/revoke/{{ $item->id }}" class="btn btn-warning btn-xs"><i class="fa fa-ban"></i></a>
                                    @endif

                                    @if($item->status==0)
                                        <a href="/dashboard/users/activate/{{ $item->id }}" class="btn btn-warning btn-xs"><i class="fa fa-thumbs-up"></i></a>
                                    @endif

                                    @if(($item->type==0) && ($item->status<>3))
                                        <a href="/dashboard/users/premote/{{ $item->id }}" class="btn btn-warning btn-xs"><i class="fa fa-level-up"></i></a>
                                    @endif

                                    @if(($item->type==1) && ($item->status<>3))
                                        <a href="/dashboard/users/demote/{{ $item->id }}" class="btn btn-warning btn-xs"><i class="fa fa-level-down"></i></a>
                                    @endif

                                    @if($item->status<>3)
                                        <a href="/dashboard/users/delete/{{ $item->id }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

    </div>

@endsection