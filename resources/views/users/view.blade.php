@extends('app')

@section('content')

    <div class="portlet light vyna-content">
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <tbody>
                    @foreach ($data as $key => $item)
                        <tr class="odd gradeX">
                            <td>{{ $key }}</td>
                            <td>{{ $item }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

    </div>

@endsection