@extends('app')

@section('page-level-css')
@stop

@section('scripts')
    @parent
@stop

@section('content')
    <form class="vyna-content-meta" method="post" action="/articles/save">
        <input type="hidden" name="id" value="{{ $data->id }}" />
        <div class="col-md-10 portlet light">
            <textarea name="content">{!! $data->content !!}</textarea>
        </div>
        <div class="col-md-2">
            <div class="form-body">
                <div class="form-group">
                    <label class="control-label">Page Title</label>
                    <input type="text" value="{{ $data->title }}" name="title" class="form-control required">
                </div>
            </div>
            <div class="form-group">
                <button id="sample_editable_1_new" class="btn btn-info content-save" href="/content/create">Save <i class="fa fa-save"></i></button>
            </div>
        </div>
    </form>

@endsection
