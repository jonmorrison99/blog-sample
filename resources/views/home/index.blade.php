@extends('home/app')

@section('content')

    @if (Session::has('message'))
        <div class="alert alert-danger">
            <strong>Whoops!</strong> {{ Session::get('message') }}<br><br>
        </div>
    @endif

    @if (!sizeof($data))
        <div class="content">
            <h3>No articles were found</h3>
            To create an article, please <a href="/signin">login</a> or <a href="/join">create an account</a>
        </div>
    @else
        <div class="portlet light vyna-content">
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Created</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($data as $item)
                        <tr class="odd gradeX">
                            <td><a href="/view/{{$item->id}}">{{ $item->title }}</a></td>
                            <td class="center">{{ $item->created }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif



@endsection
