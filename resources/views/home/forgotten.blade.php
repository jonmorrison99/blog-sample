@extends('home/app')

@section('content')
    <div class="content">
        <form class="forget-form" action="/" method="post">
            <h3>Forget Password ?</h3>
            <p>
                Enter your email address below to reset your password.
            </p>
            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="you@domain.com" name="email"/>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
            </div>
            <p><a href="/signin" class="forget-password pull-left">I remember, go to login</a></p>
        </form>
    </div>
@endsection
