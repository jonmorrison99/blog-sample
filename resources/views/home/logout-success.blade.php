@extends('home/app')

@section('content')
    <div class="content">
        <h3>You have been successfully signed out.</h3>
        To log in, either visit the home page or <a href="/">click here</a>
    </div>
@endsection
