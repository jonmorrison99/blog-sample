<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Blog - Sample Project</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="/styles/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/styles/login.css" rel="stylesheet" type="text/css"/>
    <link href="/styles/custom.css" rel="stylesheet" type="text/css"/>

    <link rel="shortcut icon" href="favicon.ico"/>
</head>

<body class="login">
    <div class="container">
        <nav>
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/signin">Login</a></li>
                <li><a href="/join">Join</a></li>
            </ul>
        </nav>

        @yield('content')

        <div class="copyright">
            <strong>&copy; <?php echo date('Y'); ?> Blood Sweat & Tea</strong> All rights reserved.
        </div>
    </div>

    <!--[if lt IE 9]>
    <script src="/js/respond.min.js"></script>
    <script src="/js/excanvas.min.js"></script>
    <![endif]-->

    <script src="/js/jquery.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{--<script src="/js/metronic.js" type="text/javascript"></script>--}}
    {{--<script src="/js/layout.js" type="text/javascript"></script>--}}
    {{--<script src="/js/demo.js" type="text/javascript"></script>--}}
    <script src="/js/login.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        jQuery(document).ready(function() {
            Login.init();
//            Metronic.init(); // init metronic core components
//            Layout.init(); // init current layout
//            Login.init();
//            Demo.init();
        });
    </script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-40029145-10', 'auto');
        ga('send', 'pageview');
    </script>
</body>
<!-- END BODY -->
</html>