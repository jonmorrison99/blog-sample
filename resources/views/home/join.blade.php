@extends('home/app')

@section('content')
    <div class="content">
        @if (Session::has('message'))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> {{ Session::get('message') }}<br><br>
            </div>
            @endif

        <form class="register-form" method="post">
            <h3 class="form-title">Join</h3>
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
			    <span>Some error message</span>
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">First</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="your first name" name="fname"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Last</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="your last name" name="lname"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Email</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="you@domain.com" name="email"/>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="your secret key" name="password"/>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-success uppercase pull-right">Join</button>
            </div>
        </form>
    </div>
@endsection
