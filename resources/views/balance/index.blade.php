@extends('app')

@section('content')
    <div class="content">
        @if(isset($message))
        <div class="alert">
            {{ $message }}
        </div>
        @endif


        <h3>Balance ${{$data->balance}}</h3>

        <form action="/dashboard/balance/process" method="post">
            <h3>Add to balance</h3>
            <p>Amount</p>
            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="$0.00" name="amount"/>
            </div>

            <p>First Name</p>
            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="" name="fname"/>
            </div>

            <p>Last Name</p>
            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="" name="lname"/>
            </div>

            <p>CCV</p>
            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="" name="ccv"/>
            </div>

            <p>Exp Month</p>
            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="" name="expm"/>
            </div>

            <p>Exp Year</p>
            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="" name="expy"/>
            </div>

            <p>Card Number</p>
            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="" name="cc"/>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-success uppercase pull-right">Process Payment</button>
            </div>
        </form>
    </div>

@endsection