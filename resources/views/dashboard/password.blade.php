@extends('app')

@section('content')
    <div class="content">
        <form action="/dashboard/password/save" method="post">
            <h3>Update Password</h3>
            <p>Enter in your new password</p>
            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="your new secret" name="secret"/>
            </div>

            <p>Enter in your current password</p>
            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="your old secret" name="password"/>
            </div>

            <div class="form-actions">
                <button type="submit" class="btn btn-success uppercase pull-right">Update</button>
            </div>
        </form>
    </div>

@endsection