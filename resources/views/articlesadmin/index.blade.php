@extends('app')

@section('content')

    <div class="portlet light vyna-content">
        <div class="portlet-body">
            @if (!sizeof($content))
            <div class="alert alert-block alert-info fade in">
                <button type="button" class="close" data-dismiss="alert"></button>
                <h4 class="alert-heading">You do not have any articles, click 'Add New' to get started.</h4>
                <p>
                    <a id="sample_editable_1_new" class="btn btn-info" href="/dashboard/articles/create">Add New <i class="fa fa-plus"></i></a>
                </p>
            </div>
            @else
            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">
                            <a id="sample_editable_1_new" class="btn btn-info" href="/dashboard/articles/create">Add New <i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>User Email</th>
                    <th>Created</th>
                    <th>Status</th>
                    <th>Tools</th>
                </tr>
                </thead>
                <tbody>

                    @foreach ($content as $item)
                        <tr class="odd gradeX">
                            <td>{{ $item->title }}</td>
                            <td>{{ $item->email }}</td>
                            <td class="center">{{ $item->created }}</td>
                            <td class="status">
                                @if($item->status==0)
                                    <button class="btn btn-info btn-xs blog-status-public" data-page="{{ $item->id }}">Private</button>
                                @elseif($item->status==1)
                                    <button class="btn btn-success btn-xs blog-status-private" data-page="{{ $item->id }}">Public</button>
                                @elseif($item->status==2)
                                    <button class="btn btn-warning btn-xs blog-status-revoked" data-page="{{ $item->id }}">Suspended</button>
                                @else
                                    <button class="btn btn-danger btn-xs blog-status-deleted" data-page="{{ $item->id }}">Deleted</button>
                                @endif
                            </td>
                            <td>
                                <div>
                                    @if($item->status<=1)
                                        <a href="/dashboard/articles/edit/{{ $item->id }}" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                                    @endif

                                    @if($item->status<2)
                                        <a href="/dashboard/articles/revoke/{{ $item->id }}" class="btn btn-warning btn-xs"><i class="fa fa-ban"></i></a>
                                    @endif

                                    @if($item->status==2)
                                        <a href="/dashboard/articles/private/{{ $item->id }}" class="btn btn-info btn-xs"><i class="fa fa-thumbs-up"></i></a>
                                    @endif

                                    @if($item->status<>3)
                                        <a href="/dashboard/articles/delete/{{ $item->id }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                                    @endif

                                    @if($item->status==0)
                                        <a href="/dashboard/articles/publish/{{ $item->id }}" class="btn btn-success btn-xs"><i class="fa fa-play"></i></a>
                                    @endif

                                    @if($item->status==1)
                                        <a href="/dashboard/articles/private/{{ $item->id }}" class="btn btn-info btn-xs"><i class="fa fa-lock"></i></a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

    </div>

@endsection