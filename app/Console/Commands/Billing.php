<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Funds;
use App\Models\Users;
use App\Models\Transactions;

class Billing extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'billing:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $users = new Users();
        $funds = new Funds();
        $log   = new Transactions();
        $amount = .25;

        foreach ($users->all() as $user) {
            $data = $funds->balance($user->id);

            if (!empty($data)) {
                $values = [
                    'user_id' => $user->id,
                    'action' => 'sub',
                    'status' => 1,
                    'log' => 'Monthly fee '. $amount,
                ];

                $amount = $data->balance - $amount;
                $funds->subtract($user->id, $amount);
                $log->log($user->id, $values);
            }
        }

        $this->info('Finished with billing');
    }
}
