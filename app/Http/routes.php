<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'guest'], function() {
    Route::get('/', 'HomeController@index');
    Route::get('/signin', 'HomeController@login');
    Route::get('/join', 'HomeController@join');
    Route::get('/view/{id}', 'HomeController@view');
    Route::get('/noarticle', 'HomeController@noarticle');
    Route::get('/nouser', 'HomeController@nouser');
    Route::get('/activated', 'HomeController@activated');
    Route::get('/user/validate/{token}', 'UsersController@isvalidate');
    Route::get('/password/reset', 'HomeController@passwordreset');
});

Route::get('/forgotten-password', 'HomeController@forgotten');

Route::group(['middleware' => ['web','auth']], function () {
    Route::auth();

    Route::get('/dashboard', 'DashboardController@index');
    Route::get('/dashboard/welcome', 'DashboardController@welcome');
    Route::get('/dashboard/password', 'DashboardController@password');
    Route::post('/dashboard/password/save', 'DashboardController@passwordupdate');

    Route::get('/dashboard/balance', 'BalanceController@index');
    Route::post('/dashboard/balance/process', 'BalanceController@process');


    Route::get('/dashboard/users', 'UsersController@index');
    Route::get('/dashboard/users/delete/{id}', 'UsersController@delete');
    Route::get('/dashboard/users/revoke/{id}', 'UsersController@revoke');
    Route::get('/dashboard/users/enable/{id}', 'UsersController@enable');
    Route::get('/dashboard/users/activate/{id}', 'UsersController@activate');
    Route::get('/dashboard/users/premote/{id}', 'UsersController@premote');
    Route::get('/dashboard/users/demote/{id}', 'UsersController@demote');
    Route::get('/dashboard/users/view/{id}', 'UsersController@view');

    Route::get('/dashboard/articles', 'ArticlesAdminController@index');
    Route::get('/dashboard/articles/create', 'ArticlesAdminController@create');
    Route::get('/dashboard/articles/edit/{id}', 'ArticlesAdminController@edit');
    Route::get('/dashboard/articles/delete/{id}', 'ArticlesAdminController@delete');
    Route::get('/dashboard/articles/revoke/{id}', 'ArticlesAdminController@revoke');
    Route::post('/dashboard/articles/save', 'ArticlesAdminController@save');
    Route::get('/dashboard/articles/publish/{id}', 'ArticlesAdminController@publish');
    Route::get('/dashboard/articles/private/{id}', 'ArticlesAdminController@unpublish');


    Route::get('/articles', 'ArticlesController@index');
    Route::get('/articles/create', 'ArticlesController@create');
    Route::get('/articles/edit/{id}', 'ArticlesController@edit');
    Route::get('/articles/delete/{id}', 'ArticlesController@delete');
    Route::post('/articles/save', 'ArticlesController@save');
    Route::get('/articles/publish/{id}', 'ArticlesController@publish');
    Route::get('/articles/private/{id}', 'ArticlesController@unpublish');
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/logout', 'Auth\AuthController@logout');
    Route::post('/auth/login', 'Auth\AuthController@login');
    Route::post('/auth/join', 'Auth\AuthController@join');
    Route::post('/auth/reset', 'Auth\AuthController@reset');
});


Route::get('/gen/{password}', function($passwd) {
    $hashed = Hash::make($passwd);

    echo $passwd . "<br />";
    echo $hashed . "<br />";

    if (Hash::check($passwd, $hashed)) {
        echo "match";
    }
});
