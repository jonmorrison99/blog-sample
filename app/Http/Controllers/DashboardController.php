<?php
namespace App\Http\Controllers;

use Auth;
use App\Models\Users;

class DashboardController extends Controller {
	public function index() {
		return view('dashboard/index');
	}

	public function welcome() {
        view()->share('data', Auth::user());

		return view('dashboard/welcome');
	}

    public function password() {
        return view('dashboard/password');
    }

    public function passwordupdate() {
        $secret = [
            'old' => trim(\Request::get('password')),
            'new' => trim(\Request::get('secret')),
        ];

        $users = new Users();

        if ($users->validateSecret(Auth::user()->id, $secret['old'])) {
            $users->updateSecret(Auth::user()->id, $secret['new']);
        }

        $user = [
            'email' => Auth::user()->email,
            'name' => Auth::user()->first_name. ' ' .Auth::user()->last_name,
        ];

        try {
            \Mail::send('emails.passwordupdated', ['user' => $user], function ($m) use ($user) {
                $m->from('lionel@morrison.mobi', 'Blog Sample');

                $m->to($user['email'], $user['name'])->subject('Blog Sample - Password Updated');
            });
        }
        catch (\GuzzleHttp\Exception\ClientException $e) {
            dd($e->getMessage());
        }

        return view('dashboard/passwordsuccess');
    }
}
