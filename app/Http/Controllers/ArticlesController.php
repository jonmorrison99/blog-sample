<?php namespace App\Http\Controllers;

use App\Models\Content;
use Illuminate\Support\Facades\Session;
use Auth;

class ArticlesController extends Controller {
	public function index() {
		$content = (new Content())->all(Auth::user()->id,1,1);

		view()->share('content', $content);

		return view('articles/index');
	}

    public function create() {
        $data = (new Content())->create(Auth::user()->id);

        return redirect()->action('ArticlesController@edit', [$data]);
    }

    public function edit($id) {
        $data = (new Content())->edit($id, Auth::user()->id);

        view()->share('data', $data);

        return view('articles/edit');
    }

    public function save() {
        $values = [
            'title' => trim(\Request::get('title')),
            'content' => trim(\Request::get('content')),
        ];

        $id = trim(\Request::get('id'));

        (new Content())->save($id, Auth::user()->id, $values);

        return redirect()->action('ArticlesController@index');
    }

    public function delete($id) {
        (new Content())->remove($id,Auth::user()->id);

        return view('articles/index');
    }

    public function publish($id) {
        (new Content())->publish($id, Auth::user()->id);

        return redirect()->action('ArticlesController@index');
    }

    public function unpublish($id) {
        (new Content())->publish($id, Auth::user()->id, 0);
        return redirect()->action('ArticlesController@index');
    }
}
