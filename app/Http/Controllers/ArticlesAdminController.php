<?php namespace App\Http\Controllers;

use App\Models\AdminContent;
use Illuminate\Support\Facades\Session;
use Auth;

class ArticlesAdminController extends Controller {
	public function index() {
		$content = (new AdminContent())->all();

		view()->share('content', $content);

		return view('articlesadmin/index');
	}

    public function create() {
        $data = (new AdminContent())->create(Auth::user()->id);

        return redirect()->action('ArticlesAdminController@edit', [$data]);
    }

    public function edit($id) {
        $data = (new AdminContent())->edit($id);

        view()->share('data', $data);

        return view('articlesadmin/edit');
    }

    public function save() {
        $values = [
            'title' => trim(\Request::get('title')),
            'content' => trim(\Request::get('content')),
        ];

        $id = trim(\Request::get('id'));

        (new AdminContent())->save($id, $values);

        return redirect()->action('ArticlesAdminController@index');
    }

    public function delete($id) {
        (new AdminContent())->remove($id);

        return redirect()->action('ArticlesAdminController@index');
    }

    public function publish($id) {
        (new AdminContent())->publish($id);

        return redirect()->action('ArticlesAdminController@index');
    }

    public function unpublish($id) {
        (new AdminContent())->publish($id, 0);
        return redirect()->action('ArticlesAdminController@index');
    }

    public function revoke($id) {
        (new AdminContent())->revoke($id);
        return redirect()->action('ArticlesAdminController@index');
    }
}
