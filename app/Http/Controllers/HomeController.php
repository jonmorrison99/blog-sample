<?php namespace App\Http\Controllers;

use App\Models\Content;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	public function index() {
		try {
			$data = (new Content())->allPublished();

			view()->share('data', $data);

			return view('home/index');
		}
		catch (\Exception $e) {
			return view('errors/405');
		}
	}

	public function login() {
		return view('home/login');
	}

	public function join() {
		return view('home/join');
	}

	public function forgotten() {
		return view('home/forgotten');
	}

	public function view($id) {
		$data = (new Content())->load($id);

		if (empty($data)) {
			return redirect()->action('HomeController@noarticle');
		}

		view()->share('data', $data);

		return view('home/view');
	}

	public function noarticle() {
		return view('home/noarticle');
	}

	public function nouser() {
		return view('home/nouser');
	}

	public function activated() {
		return view('home/activated');
	}

	public function passwordreset() {
		return view('home/passwordreset');
	}
}
