<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Users;
use App\Models\Vyna;
use Auth;
use User;

class AuthController extends Controller {
	public function login() {
		$email = \Request::get('email');
		$password = \Request::get('password');

		if ((strlen($email)>0) && (strlen($password)>0)) {
			if (Auth::attempt(['email' => $email, 'password' => $password, 'status'=>1], true)) {
				unset(Auth::user()->passwd);

                \Session::save();

				$status = [
					'status'=>'ok',
					'data'=>Auth::user(),
				];
			}
			else {
				$status = [
					'status'=>'no auth',
					'message' => 'Invalid email and/or password',
				];
			}
		}
		else {
			$status = [
				'status' => 'fail',
				'message' => 'Missing email or password attributes',
			];
		}

		return response()->json($status);
	}

    public function join() {
        $fname = trim(\Request::get('fname'));
        $lname = trim(\Request::get('lname'));
        $email = trim(\Request::get('email'));
        $password = trim(\Request::get('password'));

        if ((strlen($email)>0) && (strlen($password)>0) && (strlen($fname)>0) && (strlen($lname)>0)) {
            $user = (new Users())->byEmail($email);

            if (!empty($user)) {
                $status = [
                    'status' => 'fail',
                    'message' => 'Email exists',
                ];
            }
            else {
                $values = [
                    'email'=>$email,
                    'password'=>\Hash::make($password),
                    'first_name'=>$fname,
                    'last_name'=>$lname,
                    'joined'=>date("Y-m-d H:i:s"),
                ];

                (new Users())->create($values);

                $user = [
                    'email' => $email,
                    'name' => $fname. ' ' .$lname,
                ];

                try {
                    \Mail::send('emails.password', ['user' => $user], function ($m) use ($user) {
                        $m->from('lionel@morrison.mobi', 'Blog Sample');

                        $m->to($user['email'], $user['name'])->subject('Blog Sample - New Account Created');
                    });
                }
                catch (\GuzzleHttp\Exception\ClientException $e) {
                    $status = [
                        'status' => 'fail',
                        'message' => $e->getMessage(),
                    ];

                    return response()->json($status);
                }

                if (Auth::attempt(['email'=>$email, 'password'=>$password], true)) {
                    unset(Auth::user()->passwd);

                    \Session::save();

                    $status = [
                        'status'=>'ok',
                        'data'=>Auth::user(),
                    ];
                }
                else {
                    $status = [
                        'status'=>'no auth',
                        'message' => 'Something went wrong. Account created but unable to login in',
                    ];
                }
            }
        }
        else {
            $status = [
                'status' => 'fail',
                'message' => 'Missing fname, lname, email or password attributes',
            ];
        }

        return response()->json($status);
    }

	public function logout() {
		\Session::flush();
		Auth::logout();

		return view('/home/logout-success');
	}

    public function reset() {
        $users = new Users();
        $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $len = 10;
        $code = [];
        $charlen = strlen($characters)-1;
        $email = trim(\Request::get('email'));

        for ($i = 0; $i < $len; $i++) {
            $code[] = $characters[rand(0, $charlen)];
        }

        $secret = implode('',$code);

        $data = $users->byEmail($email);

        if (!empty($data)) {
            $users->updateSecret($data->id,$secret);

            $user = [
                'email' => $data->email,
                'name' => $data->first_name. ' ' .$data->last_name,
                'secret' => $secret,
            ];

            try {
                \Mail::send('emails.passwordreset', ['user' => $user], function ($m) use ($user) {
                    $m->from('lionel@morrison.mobi', 'Blog Sample');

                    $m->to($user['email'], $user['name'])->subject('Blog Sample - Password Reset');
                });
            }
            catch (\GuzzleHttp\Exception\ClientException $e) {
                $status = [
                    'status' => 'fail',
                    'message' => $e->getMessage(),
                ];

                return response()->json($status);
            }

            $status = [
                'status'=>'ok',
            ];
        }
        else {
            $status = [
                'status' => 'fail',
                'message' => 'Email Not Found',
            ];
        }

        return response()->json($status);
    }
}
