<?php namespace App\Http\Controllers;

use App\Models\Users;
use App\Models\Content;
use Auth;

class UsersController extends Controller {
	public function index() {
		$data = (new Users())->all();

		view()->share('data', $data);

		return view('users/index');
	}

    public function view($id) {
        $data = (new Users())->load($id);

        view()->share('data', $data);

        return view('users/view');
    }

    public function delete($id) {
        (new Users())->remove($id,Auth::user()->id);

        return redirect()->action('UsersController@index');
    }

    public function revoke($id) {
        (new Users())->revoke($id,Auth::user()->id);

        return redirect()->action('UsersController@index');
    }

    public function enable($id) {
        (new Users())->enable($id,Auth::user()->id);

        return redirect()->action('UsersController@index');
    }

    public function activate($id) {
        (new Users())->active($id,Auth::user()->id);

        return redirect()->action('UsersController@index');
    }

    public function premote($id) {
        (new Users())->admin($id,Auth::user()->id);

        return redirect()->action('UsersController@index');
    }


    public function demote($id) {
        (new Users())->user($id,Auth::user()->id);

        return redirect()->action('UsersController@index');
    }

    public function isvalidate($token) {
        $users = new Users();

        $data = $users->byEmail($token);

        if (empty($data)) {
            return redirect()->action('HomeController@index');
        }

        $users->makeactive($data->id);

        return redirect()->action('HomeController@activated');
    }
}
