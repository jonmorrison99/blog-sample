<?php namespace App\Http\Controllers;

use App\Models\Funds;
use App\Models\Transactions;
use Auth;
use App\Traits\Processor;

class BalanceController extends Controller {
	use Processor;

	public function index() {
		$data = (new Funds())->balance(Auth::user()->id);

		if (empty($data)) {
			$data = new \stdClass();
			$data->balance = 0;
		}

		$message = session('message');

		if (!is_null($message)) {
			view()->share('message', $message);
		}

		view()->share('data', $data);

		return view('balance/index');
	}

	public function process() {
		$details = [
			'ccv' => \Request::get('ccv'),
			'cc' => \Request::get('cc'),
			'expm' => \Request::get('expm'),
			'expy' => \Request::get('expy'),
			'fname' => \Request::get('fname'),
			'lname' => \Request::get('lname'),
			'amount' => \Request::get('amount'),
		];

		if (!Self::submit($details)) {
			return redirect('dashboard/balance')->with('message','Payment Failed');
		}
		else {
			$this->add($details['amount']);

			return redirect('dashboard/balance')->with('message','Payment Processed');
		}
	}

	private function add($amount) {
		$funds = new Funds();
		$log = new Transactions();

		if (is_numeric($amount)) {
			$data = $funds->balance(Auth::user()->id);

			if (empty($data)) {
				$values = [
					'user_id' => Auth::user()->id,
					'action' => 'init',
					'status' => 1,
					'log' => 'Openeing balance '. $amount,
				];

				$funds->fund(Auth::user()->id, $amount);
			}
			else {
				$values = [
					'user_id' => Auth::user()->id,
					'action' => 'add',
					'status' => 1,
					'log' => 'Depositing amount '. $amount,
				];

				$amount += $data->balance;
				$funds->add(Auth::user()->id, $amount );
			}

			$log->log(Auth::user()->id, $values);
		}
	}
}
