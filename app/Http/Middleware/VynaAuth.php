<?php namespace App\Http\Middleware;
/**
 * Created by PhpStorm.
 * User: lionel
 * Date: 4/17/15
 * Time: 6:07 PM
 */

use Closure;
use Auth;

class VynaAuth {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		if (Auth::check() === false) {
			return redirect('/signin')->with('message', 'You are not logged in.');
		}

		return $next($request);
	}

}
