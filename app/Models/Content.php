<?php
/**
 * Created by PhpStorm.
 * User: lionel
 * Date: 1/8/18
 * Time: 12:49 PM
 */

namespace app\Models;

class Content {

	/**
	 * Returns array of articles
	 *
	 * @param int $status - 0: Private, 1: Public, 2: Suspended, 3: Deleted
	 * @param int $override - return everything regardless of status
	 * @return mixed
	 */
	public static function all($uid, $status=1, $override=0) {
		if ($override) {
			$data = \DB::table('articles as a')
				->select()
                ->where('a.user_id', $uid)
				->orderBy('a.title')
				->get();
		}
		else {
			$data = \DB::table('articles as a')
				->select()
				->where('a.status', $status)
				->where('a.user_id', $uid)
				->orderBy('a.title')
				->get();
		}

		return $data;
	}

    public static function allPublished() {
        $data = \DB::table('articles as a')
            ->select()
            ->where('a.status',1)
            ->orderBy('a.title')
            ->get();

        return $data;
    }

	/**
	 * @param $siteId
	 * @param $contentType
	 * @return mixed
	 */
	public static function drafts($siteId, $contentType=0) {
		$data = \DB::table('page as p')
			->select()
			->where('p.website_idwebsite', $siteId)
			->where('p.contentType', $contentType)
			->whereNull('p.published')
			->orderBy('p.title')
			->get();

		return $data;
	}

	/**
	 * Create a new article and return the new Id
	 *
	 * @param $uid - Users id
	 * @return mixed
	 */
	public static function create($uid) {
		$values = [
			'user_id'=>$uid,
			'title'=>'New item',
			'created'=>date("Y-m-d H:i:s"),
		];

		$data = \DB::table('articles')
			->insertGetId($values);

		return $data;
	}

	/**
	 * Load the article for editing
	 *
	 * @param $id - article Id in articles table
	 * @param $uid - users Id. Prevent other users from messing with content
	 * @return mixed
	 */
	public static function edit($id, $uid) {
		$data = \DB::table('articles as a')
			->select()
			->where('a.id', $id)
			->where('a.user_id', $uid)
			->first();

		return $data;
	}

	/**
	 * Load the article
	 *
	 * @param $id
	 * @return mixed
	 */
	public static function load($id) {
		$data = \DB::table('articles as a')
			->select(['a.created','a.title','a.content'])
			->where('a.id', $id)
            ->where('a.status', 1)
			->first();

		return $data;
	}

	/**
	 * @param $id
	 * @param $uid
	 * @param $values
	 * @return mixed
	 */
	public static function save($id, $uid, $values) {
		$data = \DB::table('articles')
			->where('id', $id)
			->where('user_id', $uid)
			->update($values);

		return $data;
	}

	public static function updatePage($siteId, $id, array $values) {
		$data = \DB::table('page')
			->where('website_idwebsite', $siteId)
			->where('idpage', $id)
			->update($values);

		return $data;
	}

	/**
	 * Mark the article as public (Published)
	 *
	 * @param $id - article Id in articles table
	 * @param $uid - users Id. Prevent other users from messing with content
	 * @return mixed
	 */
	public static function publish($id, $uid, $public=1) {
		$values = [
			'status'=>$public,
		];

		$data = \DB::table('articles')
			->where('id', $id)
			->where('user_id', $uid)
			->update($values);

		return $data;
	}

	/**
	 * Mark the article as suspended. This is usually something done by an admin
	 *
	 * @param $id - article Id in articles table
	 * @param $uid - users Id. Prevent other users from messing with content
	 * @return mixed
	 */
	public static function revoke($id, $uid) {
		$values = [
			'status'=>2,
		];

		$data = \DB::table('articles')
			->where('id', $id)
			->where('user_id', $uid)
			->update($values);

		return $data;
	}

	/**
	 * Mark the article are deleted (Soft Delete)
	 *
	 * @param $id - article Id in articles table
	 * @param $uid - users Id. Prevent other users from messing with content
	 * @return mixed
	 */
	public static function remove($id, $uid) {
		$values = [
			'status'=>3,
		];

		$data = \DB::table('articles')
			->where('id', $id)
			->where('user_id', $uid)
			->update($values);

		return $data;
	}
}