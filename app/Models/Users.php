<?php
/**
 * Created by PhpStorm.
 * User: lionel
 * Date: 1/8/18
 * Time: 12:49 PM
 */

namespace app\Models;

use Hash;
use Auth;

class Users {
    /**
     * Get all the users
     *
     * @return mixed
     */
    public function all() {
        $data = \DB::table('users as u')
            ->select(['u.id','u.email','u.type','u.status','u.joined'])
            ->orderBy('u.email')
            ->get();

        return $data;
    }

    /**
     * Load the a user based on Id. Must have a status of 1 (admin) to invoke
     *
     * @return mixed
     */
    public function load($uid) {
        if ((Auth::user()->type)==1) {

            $data = \DB::table('users as u')
                ->select()
                ->where('u.id', $uid)
                ->first();

            return $data;
        }

        return null;
    }

    /**
     * Find a user by their email
     *
     * @param $email - email of the user to locate
     * @return mixed
     */
	public function byEmail($email) {
		$data = \DB::table('users as u')
			->select()
			->orderBy('u.email')
			->where('u.email', $email)
			->first();

		return $data;
	}

    /**
     * Store new user
     *
     * @param $values
     * @return mixed
     */
	public function create($values) {
		$data = \DB::table('users')
			->insertGetId($values);

		return $data;
	}

    /**
     * Validate the currant password word againt the hashed one in the database
     *
     * @param $uid - Id of the user
     * @param $secret - current secret to validate
     * @return mixed
     */
    public function validateSecret($uid,$secret) {
        $data = \DB::table('users as u')
            ->select(['u.id','u.password'])
            ->where('u.id', $uid)
            ->first();

        return Hash::check($secret, $data->password);
    }

    /**
     * Hash the secret and store
     *
     * @param $uid - Id of the user to update
     * @param $secret - new secret to store
     * @return mixed
     */
    public function updateSecret($uid,$secret) {
        $values['password'] = Hash::make($secret);

        $data = \DB::table('users as u')
            ->where('u.id', $uid)
            ->update($values);

        return $data;
    }

    /**
     * Mark the user as deleted (Soft Delete). You must have a status of 1 (admin) to invoke
     *
     * @param $uid - users Id. Prevent other users from messing with content
     * @return mixed
     */
    public static function remove($uid) {
        if ((Auth::user()->type)==1) {
            $values = [
                'status' => 3,
            ];

            $data = \DB::table('users')
                ->where('id', $uid)
                ->update($values);

            return $data;
        }

        return null;
    }

    /**
     * Mark the uses as suspended. You must have a status of 1 (admin) to invoke
     *
     * @param $uid - users Id. Prevent other users from messing with content
     * @return null
     */
    public static function revoke($uid) {
        if ((Auth::user()->type)==1) {
            $values = [
                'status' => 2,
            ];

            $data = \DB::table('users')
                ->where('id', $uid)
                ->update($values);

            return $data;
        }

        return null;
    }

    /**
     * Mark the uses as active. You must have a status of 1 (admin) to invoke
     *
     * @param $uid - users Id. Prevent other users from messing with content
     * @return null
     */
    public static function active($uid) {
        if ((Auth::user()->type)==1) {
            $values = [
                'status' => 1,
            ];

            $data = \DB::table('users')
                ->where('id', $uid)
                ->update($values);

            return $data;
        }

        return null;
    }

    /**
     * Remove user suspended/deleted status. You must have a status of 1 (admin) to invoke
     *
     * @param $uid - users Id. Prevent other users from messing with content
     * @return null
     */
    public static function enable($uid) {
        if ((Auth::user()->type)==1) {
            $values = [
                'status' => 1,
            ];

            $data = \DB::table('users')
                ->where('id', $uid)
                ->update($values);

            return $data;
        }

        return null;
    }

    /**
     * Make active
     *
     * @param $uid - users Id.
     * @return null
     */
    public static function makeactive($uid) {
        $values = [
            'status' => 1,
        ];

        $data = \DB::table('users')
            ->where('id', $uid)
            ->update($values);

        return $data;
    }
    /**
     * mark the uses as an admin. You must have a status of 1 (admin) to invoke
     *
     * @param $uid - users Id. Prevent other users from messing with content
     * @return null
     */
    public static function admin($uid) {
        if ((Auth::user()->type)==1) {
            $values = [
                'type' => 1,
            ];

            $data = \DB::table('users')
                ->where('id', $uid)
                ->update($values);

            return $data;
        }

        return null;
    }

    /**
     * mark the uses as a user. You must have a status of 1 (admin) to invoke
     *
     * @param $uid - users Id. Prevent other users from messing with content
     * @return null
     */
    public static function user($uid) {
        if ((Auth::user()->type)==1) {
            $values = [
                'type' => 0,
            ];

            $data = \DB::table('users')
                ->where('id', $uid)
                ->update($values);

            return $data;
        }

        return null;
    }
}