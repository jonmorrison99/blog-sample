<?php
/**
 * Created by PhpStorm.
 * User: lionel
 * Date: 1/8/18
 */

namespace app\Models;

class Funds {

	/**
	 * Returns account balance
	 *
	 * @param int $uid - User Id
	 * @return mixed
	 */
	public static function balance($uid) {
		$data = \DB::table('funds as f')
			->select()
			->where('f.user_id', $uid)
			->first();

		return $data;
	}

	public static function add($uid, $amount) {
		$values['balance'] = $amount;

		$data = \DB::table('funds as f')
			->where('f.user_id', $uid)
			->update($values);

		return $data;
	}

	public static function subtract($uid, $amount) {
		$values['balance'] = $amount;

		$data = \DB::table('funds as f')
			->where('f.user_id', $uid)
			->update($values);

		return $data;
	}

	public static function fund($uid, $amount) {
		$values = [
			'user_id' => $uid,
			'balance' => $amount,
		];

		$data = \DB::table('funds')
			->insertGetId($values);

		return $data;
	}
}