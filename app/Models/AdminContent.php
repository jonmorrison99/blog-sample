<?php
/**
 * Created by PhpStorm.
 * User: lionel
 * Date: 1/8/18
 * Time: 12:49 PM
 */

namespace app\Models;

use Auth;

class AdminContent {

	/**
	 * Returns array of articles
	 *
	 * @return mixed
	 */
	public static function all() {
		if (Auth::user()->type==1) {
			$data = \DB::table('articles as a')
				->join('users as u', 'u.id', '=', 'a.user_id')
				->orderBy('u.id')
				->orderBy('a.title')
				->select(['a.id', 'a.title', 'a.created', 'a.status', 'u.email'])
				->get();

			return $data;
		}

		return null;
	}

	/**
	 * @param $siteId
	 * @param $contentType
	 * @return mixed
	 */
	public static function drafts($siteId, $contentType=0) {
		if (Auth::user()->type==1) {
			$data = \DB::table('page as p')
				->select()
				->where('p.website_idwebsite', $siteId)
				->where('p.contentType', $contentType)
				->whereNull('p.published')
				->orderBy('p.title')
				->get();

			return $data;
		}

		return null;
	}

	/**
	 * Create a new article and return the new Id
	 *
	 * @param $uid - Users id
	 * @return mixed
	 */
	public static function create($uid) {
		$values = [
			'user_id'=>$uid,
			'title'=>'New item',
			'created'=>date("Y-m-d H:i:s"),
		];

		$data = \DB::table('articles')
			->insertGetId($values);

		return $data;
	}

	/**
	 * Load the article for editing
	 *
	 * @param $id - article Id in articles table
	 * @return mixed
	 */
	public static function edit($id) {
		if (Auth::user()->type==1) {
			$data = \DB::table('articles as a')
				->select()
				->where('a.id', $id)
				->first();

			return $data;
		}

		return null;
	}

	/**
	 * Load the article
	 *
	 * @param $id
	 * @return mixed
	 */
	public static function load($id) {
		if (Auth::user()->type==1) {
			$data = \DB::table('articles as a')
				->select()
				->where('a.id', $id)
				->first();

			return $data;
		}

		return null;
	}

	/**
	 * @param $id
	 * @param $values
	 * @return mixed
	 */
	public static function save($id, $values) {
		if (Auth::user()->type==1) {
			$data = \DB::table('articles')
				->where('id', $id)
				->update($values);

			return $data;
		}

		return null;
	}

	/**
	 * Mark the article as public (Published)
	 *
	 * @param $id - article Id in articles table
	 * @return mixed
	 */
	public static function publish($id, $public=1) {
		if (Auth::user()->type==1) {
			$values = [
				'status' => $public,
			];

			$data = \DB::table('articles')
				->where('id', $id)
				->update($values);

			return $data;
		}

		return null;
	}

	/**
	 * Mark the article as suspended. This is usually something done by an admin
	 *
	 * @param $id - article Id in articles table
	 * @return mixed
	 */
	public static function revoke($id) {
		if (Auth::user()->type==1) {
			$values = [
				'status' => 2,
			];

			$data = \DB::table('articles')
				->where('id', $id)
				->update($values);

			return $data;
		}

		return null;
	}

	/**
	 * Mark the article are deleted (Soft Delete)
	 *
	 * @param $id - article Id in articles table
	 * @return mixed
	 */
	public static function remove($id) {
		if (Auth::user()->type==1) {
			$values = [
				'status' => 3,
			];

			$data = \DB::table('articles')
				->where('id', $id)
				->update($values);

			return $data;
		}

		return null;
	}
}