<?php
/**
 * Created by PhpStorm.
 * User: lionel
 * Date: 1/8/18
 */

namespace app\Models;

class Transactions {
	public static function log($uid, $values) {
		$data = \DB::table('transaction_log')
			->insertGetId($values);

		return $data;
	}
}