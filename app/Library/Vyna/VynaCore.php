<?php
namespace App\Library\Vyna;

/**
 * Created by PhpStorm.
 * User: lionel
 * Date: 4/6/15
 * Time: 1:58 PM
 */

class VynaCore {
	/**
	 * @param $amount
	 * @param string $symbol
	 * @return string
	 */
	public static function money($amount, $symbol = '$') {
		if (!function_exists('money_format')) {
			return $symbol . round($amount,2);
		}
		else {
			return $symbol . money_format('%i', (double)$amount);
		}
	}
}